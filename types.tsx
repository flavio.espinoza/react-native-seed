export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  TabOne: undefined;
  TabTwo: undefined;
  Team: undefined;
  Videos: undefined;
};

export type TabOneParamList = {
  TabOneScreen: undefined;
  TeamScreen: undefined;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
  VideosScreen: undefined;
};
